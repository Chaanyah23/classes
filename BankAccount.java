public class BankAccount {

    private int balance = 0;

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        if (balance > 0) {
            this.balance += balance;
            System.out.println("Your balance has changed by " + balance + " and now it is: " + getBalance());
        }
        else {
            if (balance > getBalance()) {
                System.out.println("Transaction Fails");
            }
            else {
                System.out.println("Withdrawl of " + balance + " cannot be completed. Your balance is: " + getBalance());
            }

        }

    }

}
