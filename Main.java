public class Main {

    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount();

        bankAccount.setBalance(500);
        System.out.println(bankAccount.getBalance());
        bankAccount.setBalance(150);
        System.out.println(bankAccount.getBalance());
        bankAccount.setBalance(35);
        System.out.println(bankAccount.getBalance());
        bankAccount.setBalance(-40);
        System.out.println(bankAccount.getBalance());
        bankAccount.setBalance(-120);
        System.out.println(bankAccount.getBalance());
        bankAccount.setBalance(-900);
        System.out.println(bankAccount.getBalance());

    }

}
